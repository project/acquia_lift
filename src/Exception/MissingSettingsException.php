<?php

namespace Drupal\acquia_lift\Exception;

/**
 * Throw and exception for missing settings.
 */
class MissingSettingsException extends \Exception {}
